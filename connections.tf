terraform {
  backend "http" {

  }

  required_providers {
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~> 3.1"
    }
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region                  = "choose region"
}
