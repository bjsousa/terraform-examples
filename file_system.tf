resource "aws_efs_file_system" "site_content" {
  creation_token = "site_content"

  tags = {
    Name = "Site Content"
  }
}