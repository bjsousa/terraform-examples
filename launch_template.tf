
resource "aws_launch_template" "foo" {
  name = "foo"

  block_device_mappings {
    device_name = "/dev/sda1"

    ebs {
      volume_size = 20
    }
  }

  elastic_inference_accelerator {
    type = "eia1.medium"
  }


  image_id = "ami-08d489468314a58df"

  instance_initiated_shutdown_behavior = "terminate"

  instance_type = "t2.micro"

  key_name = "test"

  monitoring {
    enabled = true
  }

  placement {
    availability_zone = "us-west-2a"
  }

  vpc_security_group_ids = ["${aws_security_group.subnetsecurity.id}"]

  tag_specifications {
    resource_type = "instance"

    tags = {
      Name = "test"
    }
  }

  user_data = "${base64encode("#cloud-init \n - mkdir /var/www/vhosts \n- echo \"$$(curl -s http://169.254.169.254/latest/meta-data/placement/availability-zone).${aws_efs_file_system.site_content.dns_name}:/ /var/www/vhosts nfs4 fsc,noresvport,nfsvers=4.1,rsize=1048576,wsize=1048576,hard,timeo=600,retrans=2 0 0\" >> /etc/fstab \n- mount /var/www/vhosts \n- adduser wwcms -d /var/www/vhosts")}"
}


resource "aws_autoscaling_group" "bar" {
  name                      = "foobar3-terraform-test"
  max_size                  = 2
  min_size                  = 1
  health_check_grace_period = 300
  health_check_type         = "EC2"
  desired_capacity          = 2
  force_delete              = true
  vpc_zone_identifier       = ["${aws_subnet.subnet1.id}", "${aws_subnet.subnet2.id}"]


  launch_template {
    id      = "${aws_launch_template.foo.id}"
    version = "$Latest"
  }

  tag {
    key                 = "foo"
    value               = "bar"
    propagate_at_launch = true
  }

  tag {
    key                 = "lorem"
    value               = "ipsum"
    propagate_at_launch = false
  }
}